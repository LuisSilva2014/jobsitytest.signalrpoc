﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace JobSityTest.SignalRPoC.Resources.Utilities
{
    public class GenericUtils
    {

        /// <summary>
        /// </summary>
        /// <param name="inComingDate"></param>
        /// <returns></returns>
        public string GetReadableDate(DateTimeOffset _date)
        {
            DateTimeOffset newDate = TransformDateWithStandarZone(_date);

            return newDate.ToString("dddd, dd MMMM yyyy HH:mm: ss");
        }

        /// <summary>
        /// Preparate date in a standard date zone to avoid miss-understing with the server-time
        /// </summary>
        /// <param name="incoming"></param>
        /// <param name="zoneId"></param>
        /// <returns></returns>
        public DateTimeOffset TransformDateWithStandarZone(DateTimeOffset incoming, string zoneId = "SA Pacific Standard Time")
        {
            incoming = incoming.AddTicks(-(incoming.Ticks % TimeSpan.TicksPerSecond));
            incoming = TimeZoneInfo.ConvertTime(incoming, TimeZoneInfo.FindSystemTimeZoneById(zoneId));
            return incoming;
        }
    }
}
