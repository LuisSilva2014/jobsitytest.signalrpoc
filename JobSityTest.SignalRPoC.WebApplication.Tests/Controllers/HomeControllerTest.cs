﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web.Mvc;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using JobSityTest.SignalRPoC.WebApplication;
using JobSityTest.SignalRPoC.WebApplication.Controllers;
using System.Web;
using System.Security.Principal;
using System.Net;
using System.Configuration;

namespace JobSityTest.SignalRPoC.WebApplication.Tests.Controllers
{
    [TestClass]
    public class HomeControllerTest
    {

        /// <summary>
        /// Initialization of the MockHttpContextBase fake constructor
        /// </summary>
        public class MockHttpContextBase : HttpContextBase
        {
            public override IPrincipal User { get; set; }

            public HttpResponseBase Response { get; set; }
        }

        public class MockHttpResponseBase : HttpResponseBase
        {
            public  int StatusCode { get; set; }
            public  string StatusDescription { get; set; }

        }



        /// <summary>
        /// set Fake Identity In HomeController
        /// </summary>
        /// <param name="userToTest"></param>
        /// <param name="controllerContext"></param>
        private static void setFakeIdentityInHomeController(string userToTest, out ControllerContext controllerContext)
        {
            string[] roles = null;

            var fakeIdentity = new GenericIdentity(userToTest);
            var principal = new GenericPrincipal(fakeIdentity, roles);
            var fakeHttpContext = new MockHttpContextBase { User = principal };
            controllerContext = new ControllerContext
            {
                HttpContext = fakeHttpContext,
            };
        }

        #region Views Test Methods

        [TestCategory("Unit")]
        [TestMethod]
        public void IndexView_ShouldReturnUserNameInViewBag_Succesfull()
        {
            // Mock objects needs
            string userToTest = "user2";
            ControllerContext controllerContext;
            setFakeIdentityInHomeController(userToTest, out controllerContext);

            // This is the controller that we wish to test:
            var _requestController = new HomeController();

            // Now set the controller ControllerContext with fake context
            _requestController.ControllerContext = controllerContext;

            // Act
            ViewResult result = _requestController.Index() as ViewResult;

            // Assert
            Assert.IsNotNull(result);
            // Assert
            Assert.AreEqual(userToTest, result.ViewBag.userName);
        }

        [TestCategory("Unit")]
        [TestMethod]
        [ExpectedException(typeof(ArgumentException))]
        public void IndexView_ShouldReturnAnExceptionIfUserIsNotMocked()
        {
            // Arrange
            // Mock objects needs
            ControllerContext controllerContext;

            //var fakeMockHttpResponseBase = new MockHttpResponseBase(); //This object is not capable to reasign 
            var fakeHttpContext = new MockHttpContextBase();
            controllerContext = new ControllerContext
            {
                HttpContext = fakeHttpContext,
            };

            // This is the controller that we wish to test:
            var _requestController = new HomeController();

            // Now set the controller ControllerContext with fake context
            _requestController.ControllerContext = controllerContext;
            // Act
            ViewResult result = _requestController.Index() as ViewResult;
        }

        [TestCategory("Unit")]
        [TestMethod]
        public void ChatView_ShouldReturnUserNameInViewBag_Succesfull()
        {
            // Mock objects needs
            string userToTest = "user1@jobsity.com";

            ControllerContext controllerContext;
            setFakeIdentityInHomeController( userToTest, out controllerContext);

            // This is the controller that we wish to test:
            var _requestController = new HomeController();

            // Now set the controller ControllerContext with fake context
            _requestController.ControllerContext = controllerContext;

            // Act
            ViewResult result = _requestController.Chat() as ViewResult;

            // Assert
            Assert.AreEqual(userToTest, result.ViewBag.userName);
        }

        #endregion

        #region internal logic methods

        [TestCategory("Unit")]
        [TestMethod]
        public void DecoupledStockCode_Should_Retur_ExpectedJSONObject()
        {
            string _expectedMessage = "AAPL.US quote is $180.2 per share";

            // Mock objects needs
            string userToTest = "user1@jobsity.com";

            ControllerContext controllerContext;
            setFakeIdentityInHomeController(userToTest, out controllerContext);

            // This is the controller that we wish to test:
            var _requestController = new HomeController();

            // Now set the controller ControllerContext with fake context
            _requestController.ControllerContext = controllerContext;

            ConfigurationManager.AppSettings["stockEndPoint"] = "https://stooq.com/q/l/?s=STOCK_CODE";
            string stockCode = "aapl.us";
            // Act
            var result = (JsonResult) _requestController.DecoupledStockCode(stockCode);
            
            // Read the dynamic result
            object o = result.Data;
            var message = o.GetType().GetProperty("message").GetValue(o, null);

            //// Assert
            Assert.IsNotNull(result.Data);
            Assert.AreEqual(_expectedMessage, message);
        }


        #endregion

    }
}
