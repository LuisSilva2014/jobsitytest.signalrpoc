﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using JobSityTest.SignalRPoC.Repository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using JobSityTest.SignalRPoC.Repository.Interfaces;

//Use fake and stub objects
using Microsoft.QualityTools.Testing.Fakes;
using Microsoft.QualityTools.Testing.Fakes.Stubs;
using JobSityTest.SignalRPoC.Entities.Interfaces;
using JobSityTest.SignalRPoC.Entities;
using JobSityTest.SignalRPoC.Resources;
using JobSityTest.SignalRPoC.Resources.Utilities;
using JobSityTest.SignalRPoC.Repository.Interfaces.Fakes;
using JobSityTest.SignalRPoC.Repository.Fakes;

namespace JobSityTest.SignalRPoC.Repository.Tests
{
    [TestClass()]
    public class ChatRoomPostRespositoryTest
    {

        /// <summary>
        /// The DbContext
        /// </summary>
        /// <summary>
        /// Initialization of _responseService interface
        /// </summary>
        private readonly IResponseService _responseService = new ResponseService();

        /// <summary>
        /// Generic utils service
        /// </summary>
        private readonly GenericUtils _genericUtils = new GenericUtils();

      
        [TestCategory("Unit")]
        [TestMethod]
        public void GetPostsShould_ReturnData_Succesfully()
        {
            //Mock response objects
            IList<PostEntity> list = new List<PostEntity>();
            list.Add(new PostEntity()
            {
                message = "Test message",
                userName = "user",
                userTokenId = Guid.NewGuid(),
                readableDate = _genericUtils.GetReadableDate(DateTimeOffset.Now),

            });
            list.Add(new PostEntity()
            {
                message = "Test message 2",
                userName = "user 2",
                userTokenId = Guid.NewGuid(),
                readableDate = _genericUtils.GetReadableDate(DateTimeOffset.Now),
            });

            IResponseService _responseService= new ResponseService();
            _responseService.SetResponse(true, list );
            //var _chatRoomPostRespository = new ChatRoomPostRespository(context);

            //Mock repository request
            IChatRoomPostRespository _repository = new StubIChatRoomPostRespository()
            {
                GetPostsInt32 = (x) => System.Threading.Tasks.Task.FromResult(_responseService),
            };

            //Act
            var response = _repository.GetPosts(50);

            //Asserts
            Assert.IsNotNull(response);
            Assert.IsNotNull(response.Result.TotalItems == 2);
        }


        [TestCategory("Unit")]
        [TestMethod]
        [ExpectedException(typeof(Exception))]
        public void SavePostShould_Thrown_Exception()
        {
            //Mock response objects
            IList<PostEntity> list = new List<PostEntity>();

            IResponseService _responseService = new ResponseService();
            _responseService.SetResponse(true, list);
            //var _chatRoomPostRespository = new ChatRoomPostRespository(context);

            //Mock repository request
            IChatRoomPostRespository _repository = new StubIChatRoomPostRespository()
            {
                GetPostsInt32 = (x) => System.Threading.Tasks.Task.FromResult(_responseService),
                SavePostPostEntity = (_post) => { throw new Exception(Messages.AP_WEBAPI_UNEXPECTED_ERROR); }
            };

            //Act
            var response = _repository.SavePost(null);

            //Asserts
        }

        [TestCategory("Unit")]
        [TestMethod]
        public void SavePostShould_ReturnData_Succesfully()
        {
            //Mock response objects
            var _postEntity = new PostEntity()
            {
                message = "Test message",
                userName = "user",
                userTokenId = Guid.NewGuid(),
                readableDate = _genericUtils.GetReadableDate(DateTimeOffset.Now),
            };

            IResponseService _responseService = new ResponseService();
            _responseService.SetResponse(true);

            //Mock repository request
            IChatRoomPostRespository _repository = new StubIChatRoomPostRespository()
            {
                SavePostPostEntity = (_post) => System.Threading.Tasks.Task.FromResult(_responseService)
            };

            //Act
            var response = _repository.SavePost(_postEntity);

            //Asserts
            Assert.IsNotNull(response);
            Assert.IsTrue(response.Result.State);
        }
    }
}