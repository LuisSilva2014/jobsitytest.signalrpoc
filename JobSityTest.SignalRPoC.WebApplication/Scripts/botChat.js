﻿//// ===============================================================================================================
//// <copyright file="botChat.js" company="JobSity.">
//// Copyright (c) 2019
////
//// Use or copying of all or any part of the document, except as permitted 
//// by the License Agreement is prohibited.
////
//// </copyright>
//// =============================================================================================================== 
//// <summary>
//// Defines the Bot Chat namespace called "botChat".  Includes constants, and root level functions.
//// </summary>
//// =============================================================================================================== 


//// Create the botChat namespace
//(function (botChat, $, undefined) {

//    botChat.totalLoaded = 0,
//        botChat.amountRowsToDisplay = 50,
//        botChat.connection = $.connection.hub.start(),

//        botChat.init = function () {

//            //botChat.getPosts();
//        botChat.hideLoading();
//            document.getElementById(botChat.elementIds._message).focus();
//        },

//        botChat.elementIds = {
//            _displayname: "displayname",
//            _sendmessage: "sendmessage",
//            _message: "message",
//            _loader: "se-pre-con",
//            _discussion: "discussion"
//        },

//        botChat.showLoading = function () {
//            //$('#loadingImg').css('position', 'fixed').css('top', '47%').css('left', '47%').css("z-index", "1500");
//            $("." + botChat.elementIds._loader).show();
//        },
//        botChat.hideLoading = function () {
//            $("." + botChat.elementIds._loader).fadeOut("slow");
//        },

        
//        botChat.callSendMessage = function (_chat) {
//            let _msgId = '#' + this.elementIds._message;
//            // Call the Send method on the hub.
//            _chat.server.send($('#' + this.elementIds._displayname).val(), $( _msgId).val(), window.userTokenId);
//            // Clear text box and reset focus for next comment.
//            $(_msgId).val('').focus();
//        },

//        botChat.successCallBack = function (data) {

//            const array = data.result;
//            totalLoaded = data.totalItems;
//            for (var i = 0; i < array.length; i++) {
//                botChat.addToDiscussionContainer(array[i].id, array[i].userName, array[i].message, array[i].readableDate);
//            }
//            botChat.hideLoading();
//        },

//        botChat.addToDiscussionContainer = function (tempId, name, message, readableDate) {
//            let htmlNameText = name !== "Bot" ? ('<strong> ' + name + '</strong> ') : ('<span style="font-weight:600; color: cornflowerblue"> ' + name + '</span> ');
//            $("#"+ this.elementIds._discussion).append('<li> ' + tempId + '- ( ' + readableDate + ') ' + htmlNameText + ': ' + message + '</li>');
//        },

//        botChat.errorCallBack = function (jqXHR, status, error) {
//            toastr["error"](jqXHR.message === undefined ? jqXHR.statusText : jqXHR.message, "Error result");
//            botChat.hideLoading();
//        },

//        botChat.getPosts = function () {

//            botChat.showLoading();
//            $.ajax({
//                type: "GET",
//                url: window.apiBaseUrl + "/Home/GetPosts?&size=" + botChat.amountRowsToDisplay,
//                contentType: 'application/json; charset=utf-8',
//                headers: { 'userTokenId': window.userTokenId },
//                success: function (data) {
//                    if (data.success) {
//                        botChat.successCallBack(data);
//                    }
//                    else {
//                        botChat.errorCallBack(data, null, null);
//                    }
//                },
//                error: function (jqXHR, status, error) {
//                    botChat.errorCallBack(jqXHR, status, error);
//                }
//            });
//        };

//}(window.botChat = window.botChat || {}, jQuery));

//$(document).ready(function () {
//    botChat.init();
//});
