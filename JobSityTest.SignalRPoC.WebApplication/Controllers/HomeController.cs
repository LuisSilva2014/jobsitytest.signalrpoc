﻿using JobSityTest.SignalRPoC.Entities.Interfaces;
using JobSityTest.SignalRPoC.Repository;
using JobSityTest.SignalRPoC.Repository.Interfaces;
using JobSityTest.SignalRPoC.Resources;
using Microsoft.AspNet.Identity;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Net;
using System.Threading.Tasks;
using System.Web.Mvc;

namespace JobSityTest.SignalRPoC.WebApplication.Controllers
{
    /// <summary>
    /// HomeController class
    /// </summary>


    public class HomeController : Controller
    {
        /// <summary>
        /// End point complement constant
        /// </summary>
        private readonly string END_POINT_COMPLEMENT  = "&f=sd2t2ohlcv&h&e=csv";

        /// <summary>
        /// IChatRoomPostRespository object
        /// </summary>
        private readonly IChatRoomPostRespository _serviceItem = new ChatRoomPostRespository(new dbEntityContext());



        /// <summary>
        /// Load the Index view
        /// </summary>
        /// <returns></returns>
        public ActionResult Index()
        {
            try
            {
                if (User.Identity.IsAuthenticated)
                {

                    ViewBag.userName = User.Identity.Name.ToString();
                    ViewBag.userTokenId = User.Identity.GetUserId();

                    return View("Chat");
                }
                else
                {
                    return View();
                };
            }
            catch (Exception)
            {
                throw new ArgumentException(Messages.AP_WEBAPI_UNEXPECTED_ERROR);
                //throw  new Exception(Messages.AP_WEBAPI_UNEXPECTED_ERROR);
            }
           
        }

        /// <summary>
        /// Load the Chat view
        /// </summary>
        /// <returns></returns>
//#if !DEBUG
        [Authorize]
//#endif
        public ActionResult Chat()
        {
            try
            {
                ViewBag.userName = User.Identity.Name.ToString();
                ViewBag.userTokenId = User.Identity.GetUserId();
                return View();
            }
            catch (Exception)
            {
                throw new ArgumentException(Messages.AP_WEBAPI_UNEXPECTED_ERROR);
            }
        }

        /// <summary>
        /// Get the recenltly 50 posts
        /// </summary>
        /// <param name="stockCode"></param>
        /// <returns></returns>
        [HttpGet]
        public async Task<JsonResult> GetPosts(int size)
        {
            try
            {
                ///-----------
                var userTokenId = Request.Headers["userTokenId"] ?? string.Empty;
                Guid sessionToken;
                //TODO: once you obtain a session Guid, you can create a filterApi in charge of verificate the sessionguid provide in the database,
                // Then you can reuse this functionality

                if (String.IsNullOrWhiteSpace(userTokenId) || !Guid.TryParse(userTokenId, out sessionToken))
                    return Json(new { message = Messages.AP_WEBAPI_SESSION_GUID_NOT_PROVIDED, status = (int)HttpStatusCode.BadRequest }, JsonRequestBehavior.AllowGet);

                if (!User.Identity.GetUserId().Equals(sessionToken.ToString()))
                {
                    if (String.IsNullOrWhiteSpace(userTokenId) || !Guid.TryParse(userTokenId, out sessionToken))
                        return Json(new { message = Messages.AP_WEBAPI_SESSION_GUID_NOT_PROVIDED, status = (int)HttpStatusCode.Unauthorized }, JsonRequestBehavior.AllowGet);
                }
                //-----


                IResponseService response = await _serviceItem.GetPosts(size).ConfigureAwait(false);
                return Json(new
                {
                    success = response.Result != null ? true : false,
                    result = response.TotalItems > 0 ? response.Result : null,
                    totalItems = response.TotalItems,
                    message = string.Empty,
                }, JsonRequestBehavior.AllowGet);

            }
            catch (Exception)
            {
                return Json(new { message = Messages.AP_WEBAPI_UNEXPECTED_ERROR, statusCode = (int)HttpStatusCode.InternalServerError }, JsonRequestBehavior.AllowGet);
            }
        }


        /// <summary>
        /// Decopled stock code
        /// </summary>
        /// <param name="stockCode"></param>
        /// <returns></returns>
        [HttpGet]
        [Route("_decoupledStockCode")]
        public JsonResult DecoupledStockCode(string stockCode)
        {
            try
            {
                string endPointUrl = ConfigurationManager.AppSettings["stockEndPoint"].ToString().TrimEnd('/').Replace("STOCK_CODE", stockCode) + END_POINT_COMPLEMENT;
                var response = DecodeCSVFile(endPointUrl);

                string messageComposed = stockCode.ToUpper() + " quote is $" + (response == null ? string.Empty : response[10].ToString()) + " per share";
                return Json(new
                {
                    success = response != null ? true : false,
                    result = response ?? response,
                    message = messageComposed,
                }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception)
            {
                return Json(new { message = Messages.AP_WEBAPI_UNEXPECTED_ERROR , statusCode= (int)HttpStatusCode.InternalServerError }, JsonRequestBehavior.AllowGet);
            }
        }

        /// <summary>
        /// CSV Builder
        /// </summary>
        /// <param name="url"></param>
        /// <returns></returns>
        private string GetCSV(string url)
        {
            HttpWebRequest req = (HttpWebRequest)WebRequest.Create(url);
            HttpWebResponse resp = (HttpWebResponse)req.GetResponse();

            StreamReader sr = new StreamReader(resp.GetResponseStream());
            string results = sr.ReadToEnd();
            sr.Close();

            return results;
        }

        /// <summary>
        /// Decoder of CSV data
        /// </summary>
        /// <param name="url"></param>
        /// <returns></returns>
        private List<string> DecodeCSVFile(string url)
        {
            List<string> splitted = new List<string>();
            string fileList = GetCSV(url);
            string[] tempStr;

            tempStr = fileList.Split(',');

            foreach (string item in tempStr)
            {
                if (!string.IsNullOrWhiteSpace(item))
                {
                    splitted.Add(item);
                }
            }

            return splitted;
        }
    }

}