﻿using Microsoft.AspNet.SignalR;
using Microsoft.Owin;
using Owin;
[assembly: OwinStartup(typeof(JobSityTest.SignalRPoC.WebApplication.Startup))]

namespace JobSityTest.SignalRPoC.WebApplication
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);

            //GlobalHost.HubPipeline.AddModule(new ErrorHandlingPipelineModule());


            //Initialize the MapSignalR global function 
            app.MapSignalR();
        }
    }
}
