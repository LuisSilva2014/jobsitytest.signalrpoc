﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using JobSityTest.SignalRPoC.Repository;
using JobSityTest.SignalRPoC.Repository.Interfaces;
using JobSityTest.SignalRPoC.Resources;
using JobSityTest.SignalRPoC.Resources.Utilities;
using JobSityTest.SignalRPoC.WebApplication.Controllers;
using Microsoft.AspNet.SignalR;

namespace JobSityTest.SignalRPoC.WebApplication.Hubs
{
    public class ChatHub : Hub
    {

        /// <summary>
        /// IChatRoomPostRespository object
        /// </summary>
        private readonly IChatRoomPostRespository _serviceItem = new ChatRoomPostRespository(new dbEntityContext());

        /// <summary>
        /// Generic utils service
        /// </summary>
        private readonly GenericUtils _genericUtils = new GenericUtils();

        /// <summary>
        /// Command list available
        /// </summary>
        private string[] commandListAvailable = { "/stock=" }; //Ex: { "command1", "command2" ... }

        /// <summary>
        /// Hello
        /// </summary>
        public void Hello()
        {
            Clients.All.hello();
        }

        /// <summary>
        /// Post Sender 
        /// </summary>
        /// <param name="name">User name</param>
        /// <param name="message">User Message </param>
        public void Send(string name, string message, Guid userToken)
        {
            try
            {
                string readableDate = _genericUtils.GetReadableDate(DateTimeOffset.Now);
                // Log user request
                Clients.All.addNewMessageToPage(name, message, "", readableDate);

                //Store in the repository the user post - exclude command and bot request
                var isCommandIntheList = commandListAvailable.Where(x => message.ToLower().Contains(x)).FirstOrDefault();

                if (!name.Equals("Bot") && isCommandIntheList == null)
                {
                    _serviceItem.SavePost(new Entities.PostEntity() { message = message, userName = name, userTokenId = userToken });
                }

                //Execute the command requested
                if (isCommandIntheList != null)
                {
                    switch (isCommandIntheList)
                    {
                        case "/stock=":
                            ExecuteStockCommand(message, readableDate);
                            break;
                        default:
                            //Here another conditional for future command
                            break;
                    }
                }
            }
            catch (Exception)
            {
                throw new HubException(Messages.AP_WEBAPI_UNEXPECTED_ERROR);
            }
           

        }

        /// <summary>
        /// ExecuteStockCommand method
        /// </summary>
        /// <param name="message"></param>
        /// <param name="readableDate"></param>
        /// <returns></returns>
        private void ExecuteStockCommand(string message, string readableDate)
        {
            string codeToSearch = string.Empty;
            //Extract only the code request
            string[] array = message.Split(' ');

            var resp1 = array.Where(x => x.ToLower().Contains("/stock=")).First();

            var resp2 = resp1.Split('=');
            codeToSearch = resp2[1] ?? resp2[1];

            dynamic responseEndPoint = new HomeController().DecoupledStockCode(codeToSearch);

            //Log Bot response
            Clients.All.addNewMessageToPage("Bot", responseEndPoint.Data ?? responseEndPoint.Data, codeToSearch, readableDate);
        }

    }

}