===========================================================
Author: Luis Silva
Profession: Software enginner
Linkeind page: https://www.linkedin.com/in/luis-silva-spm/
Personal website: http://silva-solutions.com
===========================================================
[Goal]: Chat Room Project to evaluate Luis silva technical experience in .net

* Solution name: JobSityTest.SignalRPoC.sln
* Dev environment:  http://luis-silva-temp2.azurewebsites.net/
* User availables for testing: (both use the same password: Hello12#)
  - user1@jobsity.com
  - user2@jobsity.com

======= Version control link (With Azure VSTF and GitLab) ==================
I managed all the task and likely item that was need in a repository in azure. So all the code and progress board you can find in this link:
https://dev.azure.com/silva-solutions/LuisSilvaJobSityTest.SignalRPoC.ChatsBot
If you cannot access or see any code, please request access pressing click on the SingIn button, it should give you acces, however if not, pelase email me with your email, and I can added you manually

Also, and alternative repository is at my GitLab account:
https://gitlab.com/LuisSilva2014/jobsitytest.signalrpoc.git

======= Steps to run the project locally ==================
1) Pull/download the entire code of the solution 
2) Open, the sln file
3) Restore all the nuggets needed
4) Build the solution
5) Start solution pressing F5 key
6) To recreate user post, I recommend creating/sign up with a new user
7) Done, Enjoy!

======= Credentials of the remote database ================
*Purpose: Dev-Testing
*Server: sql5037.site4now.net
*User: DB_9D8E17_jobsity_admin
*Password: Hello12#

 ======= Project Insights =====================================
* The architecture of the solution is n-layer
* I am exposing some APIs in the higher layer and encapsulating data access in the lowest one.
* A transversal communication through interfaces and entities objects are implemented to communicate the repository layer response to the application layer.
* To handle user real-time communication, I implemented the Signal R as message broker, it's like a facilitator of the user posts between the  UI and back end, 
since the hub is in charge to request and response those requested objects.
* To garantee confidential access, I implemented the default authentication method called Identity, this is provided for MVC template applications.
  -This one,  allows requests to travel in a secure way in our application.
* Also, I made sure that most of the ajax requests need in their header section, the userTokenId to complete the request, otherwise, the action method will reject the request.
* To manage the database I used Entity Framework in the model-first alternative.
* The message field of the chatRoom is able to interpret the command: /stock=aapl.us   . So if the user types this test, the app will fetch data from an external resource and then communicate that to the user. 
* Unit test to evaluate the repository and application methods are provided in the test project, because of time, there is only a code-coverage of the 16.32%. Build unit test consume a too much time!!
* I tried to set a continuous integration with the Azure Build feature, but it was consuming me too much time, So That's why I decide to do not implement it. Maybe at a later time!
 Public link: https://dev.azure.com/silva-solutions/LuisSilvaJobSityTest.SignalRPoC.ChatsBot/_build?definitionId=11
* 
[Bones] The chatroom has a bonus validation when the internet has gone, so the bot will communicate to the user that there is an issue with it
---------------------
----------------------
----------------------------
Please dot not copy unless I authorize, This is part of the technical test of Luis Silva for the Senior .Net develper position at JobSity
