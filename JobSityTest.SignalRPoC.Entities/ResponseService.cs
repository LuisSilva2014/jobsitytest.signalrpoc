﻿using JobSityTest.SignalRPoC.Entities.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace JobSityTest.SignalRPoC.Entities
{
    public class ResponseService : IResponseService
    {
        public List<string> Errors { get; set; }

        public dynamic Result { get; set; }

        public bool State { get; set; }
        public int TotalResult { get; set; }
        public int TotalItems { get; set; }

        public ResponseService()
        {
            State = false;
            Errors = new List<string>();
            TotalItems = 0;
            TotalResult = 0;
        }

        public void SetResponse(bool state, dynamic result = null, List<string> errors = null, int totalItems = 0, int totalResult = 0)
        {
            State = state;
            Result = result;
            Errors = errors;
            TotalItems = totalItems;
            TotalResult = totalResult;
        }
    }

}