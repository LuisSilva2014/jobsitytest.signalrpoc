﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace JobSityTest.SignalRPoC.Entities.Interfaces
{
   
        /// <summary>
        /// Response generic class
        /// </summary>
        public interface IResponseService
        {
            List<string> Errors { get; set; }

            dynamic Result { get; set; }

            int TotalItems { get; set; }
            int TotalResult { get; set; }
            bool State { get; set; }

            void SetResponse(bool state = false, dynamic result = null, List<string> errors = null, int totalItems = 0, int totalResult = 0);
        }
    }
