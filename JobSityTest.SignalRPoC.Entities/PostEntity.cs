﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace JobSityTest.SignalRPoC.Entities
{
    public class PostEntity
    {
        /// <summary>
        /// Id
        /// </summary>
        public int id { get; set; }
        /// <summary>
        /// User name
        /// </summary>
        public string userName { get; set; }

        /// <summary>
        /// Message of the post
        /// </summary>
        public string message { get; set; }

        /// <summary>
        /// User token id
        /// </summary>
        public Guid userTokenId { get; set; }

        /// <summary>
        /// Date creation
        /// </summary>
        public string readableDate { get; set; }
    }
}
