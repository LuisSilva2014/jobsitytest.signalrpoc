-- ==========================================================================
--  Copyright 2019 JobSity, LLC.
--    Use or copying of all or any part of the document, except as permitted
--    by the License Agreement is prohibited.
-- ==========================================================================
-- File:   tblChatRoomPosts.sql
-- ==========================================================================
--    Revision History
-- ----------------------------------------------------------------------------------
--  Date		User id     Reason for change
--  ---------   ----------- ---------------------------------------------------------
--  2019May26   luisSilva	Original 
-- ==================================================================================

--select *
--from INFORMATION_SCHEMA.COLUMNS
--where TABLE_NAME='AspNetUsers'


--ALTER TABLE tblChatRoomPosts
--ADD CONSTRAINT [PostIdKey] PRIMARY KEY(PostId)
--DROP TABLE tblChatRoomPosts
-- SELECT * FROM tblChatRoomPosts
CREATE TABLE tblChatRoomPosts
(
	[PostId]				BIGINT	IDENTITY(1,1)			NOT NULL  PRIMARY KEY,
	[UserIdToken]			NVARCHAR(128) NULL,
	[CreateAt]				DATETIMEOFFSET	NULL,
	[Message]				NVARCHAR(MAX)	NULL,
	[UserName]				NVARCHAR(30)	NULL,
	CONSTRAINT [FK_tblChatRoom_User_] FOREIGN KEY (UserIdToken) REFERENCES AspNetUsers(Id)
)
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', 
	@value=N' System Type Identifier' , 
	@level0type=N'SCHEMA',
	@level0name=N'dbo', 
	@level1type=N'TABLE',
	@level1name=N'tblChatRoomPosts', 
	@level2type=N'COLUMN',
	@level2name='PostId'
GO
EXEC sp_addextendedproperty @name = N'MS_Description',
    @value = N' User Session Token',
    @level0type = N'SCHEMA',
    @level0name = N'dbo',
    @level1type = N'TABLE',
    @level1name = N'tblChatRoomPosts',
    @level2type = N'COLUMN',
    @level2name = N'UserIdToken'
GO
EXEC sp_addextendedproperty @name = N'MS_Description',
    @value = N' Log of the post',
    @level0type = N'SCHEMA',
    @level0name = N'dbo',
    @level1type = N'TABLE',
    @level1name = N'tblChatRoomPosts',
    @level2type = N'COLUMN',
    @level2name = N'CreateAt'
GO
EXEC sp_addextendedproperty @name = N'MS_Description',
    @value = N' Message of the post',
    @level0type = N'SCHEMA',
    @level0name = N'dbo',
    @level1type = N'TABLE',
    @level1name = N'tblChatRoomPosts',
    @level2type = N'COLUMN',
    @level2name = N'Message'
GO
EXEC sp_addextendedproperty @name = N'MS_Description',
    @value = N' User Name ',
    @level0type = N'SCHEMA',
    @level0name = N'dbo',
    @level1type = N'TABLE',
    @level1name = N'tblChatRoomPosts',
    @level2type = N'COLUMN',
    @level2name = N'UserName'
GO