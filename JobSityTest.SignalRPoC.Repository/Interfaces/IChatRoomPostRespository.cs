﻿using JobSityTest.SignalRPoC.Entities;
using JobSityTest.SignalRPoC.Entities.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace JobSityTest.SignalRPoC.Repository.Interfaces
{
    public interface IChatRoomPostRespository
    {

        /// <summary>
        /// Save post
        /// </summary>
        /// <param name="post"></param>
        /// <returns></returns>
        Task<IResponseService> SavePost(PostEntity post);
        /// <summary>
        /// Get the lastest post of the room
        /// </summary>
        /// <param name="size">Example 50</param>
        /// <returns></returns>
        Task<IResponseService> GetPosts(int size);
    }
}
