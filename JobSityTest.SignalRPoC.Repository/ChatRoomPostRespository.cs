﻿using JobSityTest.SignalRPoC.Entities;
using JobSityTest.SignalRPoC.Entities.Interfaces;
using JobSityTest.SignalRPoC.Repository.Interfaces;
using JobSityTest.SignalRPoC.Resources.Utilities;
using JobSityTest.SignalRPoC.Resources;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;

namespace JobSityTest.SignalRPoC.Repository
{
    public class ChatRoomPostRespository : IChatRoomPostRespository
    {
        /// <summary>
        /// The DbContext
        /// </summary>
        private readonly dbEntityContext context; // = new dbEntityContext();
        /// <summary>
        /// Initialization of _responseService interface
        /// </summary>
        private readonly IResponseService _responseService = new ResponseService();

		/// <summary>
        /// Generic utils service
        /// </summary>
        private readonly GenericUtils _genericUtils = new GenericUtils();

     
        /// <summary>
        /// Contructor
        /// </summary>
        /// <param name="contextObj"></param>
        public ChatRoomPostRespository(dbEntityContext contextObj)
        {
            context = contextObj;
        }


        /// <summary>
        /// Save post
        /// </summary>
        /// <param name="post"></param>
        /// <returns></returns>
        public async Task<IResponseService> SavePost(PostEntity post)
        {
            try
            {
                context.tblChatRoomPosts.Add(new tblChatRoomPost()
                {
                    UserName = post.userName,
                    UserIdToken = post.userTokenId.ToString(),
                    CreateAt = _genericUtils.TransformDateWithStandarZone(DateTimeOffset.Now),
                    Message = post.message,
                });
                _responseService.SetResponse(await context.SaveChangesAsync() > 0);
                return _responseService;
            }
            catch (Exception)
            {
                //TODO: Log error in the database to track errors
                throw new Exception(Messages.AP_WEBAPI_ERROR_CONECTING_TO_REMOTE_DATABASE);
            }
        }

        /// <summary>
        /// Get the lastest post of the room
        /// </summary>
        /// <param name="size">Example 50</param>
        /// <returns></returns>
        public async Task<IResponseService> GetPosts(int size)
        {
            try
            {
                int totalItems = 0;
                IList<tblChatRoomPost> records = new List<tblChatRoomPost>();

                using (var context = new dbEntityContext())
                {
                    records = context.tblChatRoomPosts.ToList()
                        .OrderByDescending(x => x.CreateAt).ToList()
                        .Take(size).ToList();
                    totalItems = records.Count();
                    await context.SaveChangesAsync(); //This line is not need it, but due to we want an asyn method we have to set
                }

                //Let's trasform data object to an entityObject
                if (records != null && records.Any())
                {
                    int i = 1;
                    List<PostEntity> itemsMapped = (from n in records
                                                    orderby n.CreateAt
                                                    select new PostEntity
                                                    {
                                                        id = i++,
                                                        readableDate = _genericUtils.GetReadableDate(n.CreateAt.Value),
                                                        message = n.Message.ToString(),
                                                        userName = n.UserName.ToString(),
                                                        userTokenId = new Guid(n.UserIdToken)
                                                    }
                   ).ToList();

                    _responseService.SetResponse(true, itemsMapped, null, totalItems);
                    _responseService.State = true;
                }
                return _responseService;
            }
            catch (Exception)
            {
                //TODO: Log error in the database to track errors
                throw new Exception(Messages.AP_WEBAPI_ERROR_CONECTING_TO_REMOTE_DATABASE);
            }
        }

    }
	 
}